from measure2d import *
import matplotlib.pyplot as plt
from scipy.optimize import differential_evolution, basinhopping
def func(x):
    '''Objective function for differential evolution optimizer'''
    return cp.get_coefficient(x[0], x[1], x[2])

def calculate_CC(cp,bnds,maxiter=None):
    '''calculates chiral coefficient for a chiral_polygon object
    given max iterations and translational/rotational bounds'''
    result = differential_evolution(func, bnds, disp=False,maxiter=maxiter, \
                                    popsize=20,tol=0.0001)
    coefficient = float(result['fun'])
    rotation = result.x[0]
    translation = result.x[1:]
    overlap_polygon = cp.get_overlap(rotation, translation, False, True)
    return [coefficient,translation,rotation]

def make_L(d,l,A):
    '''make an L-shape with h parameter determined by total area.
    d is the thickness and l is the length of one leg.'''
    h=A/d-l
    pt_list = [
        (0, 0),
        (l+d, 0),
        (l+d, d),
        (d, d),
        (d, h),
        (0, h),
        (0, 0)
        ]
    global cp
    cp = Chiral_polygon(pt_list, rounding=False)
    cp.create_mirror_image(-1.0,1.0,[2*d,0.0,0.0])
    return cp

def make_dimer(a,b,d,r):
    '''Make a dimer as the union of two rectangles.
    width/height of the rectangles are a and b
    and the vertical and horizontal spacing are d and r'''
    pt_list_1 = [
        (a, b),
        (a, 0),
        (0, 0),
        (0, b),
        (a, b)
        ]
    pt_list_2 = [
        (r, d),
        (r, d+b),
        (a+r, d+b),
        (a+r, d),
        (r, d)
        ]
    P_1 = Polygon(pt_list_1)
    P_2 = Polygon(pt_list_2)
    global cp
    cp = Chiral_polygon(pt_list_1)
    cp.polygon = P_1.union(P_2)
    cp.create_mirror_image(-1.0, 1.0, [(a+r)/2, 0.0, 0.0]) 
    return cp

def make_tri(x,y,b):
    '''make a triangle given the position of the top vertex (x,y)
    and the base length, b'''
    pt_list = [
            (x,y),
            (b,0),
            (0,0)
            ]
    global cp
    cp = Chiral_polygon(pt_list)
    cp.create_mirror_image(-1.0, 1.0, [b/2.0, 0.0, 0.0])
    return cp

#############Some Examples###########

####dx,dy are the translations and dtheta is the rotation
####for the configuration of maximum overlap of mirror images.

####each of these examples calculates chiral coefficient (cc)
####for several geometries of a given shape and then plots the
####optimized configuration of the last pair of shapes analyzed.

def Tri_CC():
    print("x\tcc\tdx\tdy\tdtheta")
    for x in range(-100,200,5):
        _x=x/1000.0
        cp = make_tri(_x,0.2,0.4)
        cc,tr,rot = calculate_CC(cp,((-2*np.pi, 0), (-1.0,1.0), (-1.0,1.0)),10000)
        print("%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f" % (_x,cc,tr[0],tr[1],rot))
    print("plotting max overlap of last config")
    maxoverlap = cp.get_overlap(rot,tr, return_shape=True)
    plot_polygons([cp.polygon,maxoverlap])
    
def Dimer_CC_d():
    print("d\tcc\tdx\tdy\tdtheta")
    for r in range(280,330,5):
        _r=r/1000.0
        cp = make_dimer(0.1,0.3,0.2,_r)
        cc,tr,rot = calculate_CC(cp,((-2*np.pi, 0), (-1.0,1.0), (-1.0,1.0)),10000)
        print("%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f" % (_r,cc,tr[0],tr[1],rot))
    print("plotting max overlap of last config")
    maxoverlap = cp.get_overlap(rot,tr, return_shape=True)
    plot_polygons([cp.polygon,maxoverlap])
    
def Dimer_CC_r():
    print("d\tcc\tdx\tdy\tdtheta")
    for d in range(30,300,5):
        _d=d/1000.0
        cp = make_dimer(0.1,0.3,_d,.1)
        cc,tr,rot = calculate_CC(cp,((-2*np.pi, 0), (-1.0,1.0), (-1.0,1.0)),10000)
        print("%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f" % (_d,cc,tr[0],tr[1],rot))
    print("plotting max overlap of last config")
    maxoverlap = cp.get_overlap(rot,tr, return_shape=True)
    plot_polygons([cp.polygon,maxoverlap])
    
def L_CC():
    print("d\tcc\tdx\tdy\tdtheta")
    l=100.0/1000.0
    for d in range(100,160,20):
        _d=d/1000.0
        cp= make_L(_d,l,.057)
        cc,tr,rot = calculate_CC(cp,((-2*np.pi, 0), (-1.0,1.0), (-1.0,1.0)),10000)
        print("%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f" % (_d,cc,tr[0],tr[1],rot))
    print("plotting max overlap of last config")
    maxoverlap = cp.get_overlap(rot,tr, return_shape=True)
    plot_polygons([cp.polygon,maxoverlap])

