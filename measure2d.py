import numpy as np
from scipy.optimize import minimize

from shapely.geometry import Polygon, MultiPolygon
from shapely import affinity, ops

import matplotlib.pyplot as plt
from descartes.patch import PolygonPatch



class Chiral_polygon(object):
    '''Chiral polygon class used to contain the information for a 2D polygon and its mirror image.'''
    def __init__(self, pt_list, rounding=False):
        if rounding:
            x = Polygon(pt_list)
            d = 0.1
            res = 2
            y = x.buffer(-d, resolution=res).buffer(d, resolution=res)
            self.polygon = y.buffer(d, resolution=res).buffer(-d, resolution=res)
        else:
            self.polygon = Polygon(pt_list)
        self.m_polygon = None

    def create_mirror_image(self, xf=1.0, yf=-1.0, center=[0.0, 0.0, 0.0]):
        """
        Mirrors self.polygon
        TODO: replace scaling by simple matrix mirror op
        """
        self.m_polygon = affinity.scale(self.polygon, xfact=xf, yfact=yf, origin=center)

    def get_overlap(self,
                    rotation,
                    translation,
                    show_overlap=False,
                    return_shape=False):
        """
        1. rotate
        2. translate
        3. intersect
        """
        _r_polygon = affinity.rotate(self.m_polygon,
                                     rotation,
                                     origin='center',
                                     use_radians=True)

        trnsl_func = lambda x, y: (x + translation[0], y + translation[1])
        _t_polygon = ops.transform(trnsl_func, _r_polygon)

        intersection = self.polygon.intersection(_t_polygon)

        if return_shape:
            #return intersection
            return _t_polygon
        else:
            return self._area(intersection)

    def _area(self, polygon):
        """
        If the intersection of two polygons has dimension < 2,
        the type becomes GeometryCollection, which has no area.
        """
        if type(polygon) == Polygon or type(polygon) == MultiPolygon:
            return polygon.area
        else:
            return 0.0

    def get_coefficient(self, alpha, x, y):
        """
        To be minimized
        """
        v_overlap = self.get_overlap(alpha, [x, y])
        return 1 - v_overlap / self.polygon.area

def plot_polygons(polygon_list,x_range=[-1,1],y_range=[-1,1]):
    COLOR = {
        True:  '#6699cc',
        False: '#ff3333'
        }

    def v_color(ob):
        return COLOR[ob.is_valid]

    fig = plt.figure(1)

    ax = fig.add_subplot(111)
    colors = {'1':'#ff0000','2':'#0000ff','3':'#000080'}
    i=1
    for polygon in polygon_list:
        if type(polygon) == MultiPolygon:
            for pol in polygon.geoms:
                
                X, Y = pol.exterior.xy
                ax.plot(X, Y, '.', color=colors[str(i)], zorder=1)
                patch = PolygonPatch(pol,
                                     facecolor=colors[str(i)],
                                     edgecolor=colors[str(i)],
                                     alpha=0.5,
                                     zorder=2)
                ax.add_patch(patch)
        else:
            X, Y = polygon.exterior.xy
            ax.plot(X, Y, '.', color='#999999', zorder=1)
            patch = PolygonPatch(polygon,
                                 facecolor=colors[str(i)],
                                 edgecolor=colors[str(i)],
                                 alpha=0.5,
                                 zorder=2)
            ax.add_patch(patch)
        i+=1
    ax.set_xlim(*x_range)
    ax.set_xticks([i+x_range[-1] for i in range(*x_range)])
    ax.set_ylim(*y_range)
    ax.set_yticks([i+y_range[-1] for i in range(*y_range)])
    ax.set_aspect(1)

    plt.show()
    

if __name__ == '__main__':
    pt_list = [
        (0,0),
        (.5,0),
        (.5,.5),
        (0,1)
        ]

    cp = Chiral_polygon(pt_list)
    cp.create_mirror_image()
    
    x = cp.get_overlap(np.pi/8,[0,.5], return_shape=True)

    p = Polygon(pt_list)
    
    q = affinity.scale(p, xfact=1.0, yfact=-1.0)

    pq = p.intersection(q)

    plot_polygons([cp.polygon,x])

